<?php
function generate($n) 
{ 
    $k = 0; 
    $alpha = range('A', 'Z');
    for ($i = 1; $i <= $n; $i++)
    {
        if($n % 2 != 0){
            if ($i % 2 != 0) 
            { 
                for($j=$k + 1;$j<=$i + 1;$j++) 
                {
                    if ($j == 1) continue;
                    echo $j;	 
                }
                echo "<br/>"; 
            }else{
                for($j=$k; $j<$i; $j++){ 
                     if($j % 2  == 0){
                         echo strtolower($alpha[$j]);      
                     }else{
                        echo $alpha[$j];  
                    }
                }  
                echo "<br>";  
            }
        }else{
            if ($i % 2 != 0) 
            { 
                for($j=$k; $j<$i; $j++){  
                    if($j % 2  == 0){
                        echo strtolower($alpha[$j]);      
                    }else{
                       echo $alpha[$j];  
                   }  
                }  
                echo "<br>";     
            }else{
                for($j=$k + 1;$j<=$i + 1;$j++) 
                {
                    if ($j == 1) continue;
                    echo $j;	 
                }
                echo "<br/>"; 
            }
        }
    } 
} 
?>
<html>
    <head>
      <style>
         .error {color: #FF0000;}
      </style>
   </head>
   
   <body>
      <?php
         $inputErr = "";
         $input = "";
         
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["input"])) {
               $inputErr = "Input is required";
            }elseif(!is_numeric($_POST["input"])){
                $inputErr = "Input should be numeric";
            }
         }
      ?>
      <h2>Program 1</h2>
      <p><span class = "error">* required field.</span></p>
      <form method = "post" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <table>
            <tr>
               <td>Input:</td>
               <td><input type = "text" name = "input">
                  <span class = "error">* <?php echo $inputErr;?></span>
               </td>
            </tr>
            <td>
               <input type = "submit" name = "submit" value = "Submit"> 
            </td>
         </table>
      </form>
      <?php
         echo "<h2>Your given values are as:</h2>";
         $input = generate($_POST["input"]);
         echo $input;
         echo "<br>";
      ?>
   </body>
</html>