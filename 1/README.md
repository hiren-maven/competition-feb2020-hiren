# Project Title

Write a program to create pattern as per below sample
Note: Last row should always contain numbers.

Example 1:
Input: 5
Output
2
aB
234
aBcD
23456
Example 2:
Input: 6
Output
a
23
aBC
2345
aBcDe
234567

Important Note

If you are developing this programme in PHP / .NET then it must be web-based. CLI is not allowed
If you are developing this programme in Any JS Framework then it must be web-based. CLI is not allowed. Input and Output must work with JSON file like API call.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

- Download this files on your local machine where your server is running

## Running the tests

Run http://localhost/competition/1/index.php in browser. ( You can use your server url instead of localhost )
