<?php
function generate(&$c, $n) 
{ 
    $X = array( 0, -1, 0, 1 ); 
    $Y = array( 1, 0, -1, 0 ); 
    $isValid = true; 
  
    // Traversing each cell  
    // of the chess board. 
    for ($i = 0; $i < $n; $i++) 
    { 
        for ($j = 0; $j < $n; $j++) 
        { 
            // for each adjacent cells 
            for ($k = 0; $k < 4; $k++)  
            { 
                $newX = $i + $X[$k]; 
                $newY = $j + $Y[$k]; 
  
                // checking if they have different color 
                if ($newX < $n && $newY < $n &&  
                    $newX >= 0 && $newY >= 0 &&  
                    $c[$newX][$newY] == $c[$i][$j])  
                { 
                    $isValid = false; 
                } 
            } 
        } 
    } 
    return $isValid; 
} 
function generate_board($n){
    for ($i=1; $i<=$n; $i++)  
    {  
        for ($j=1; $j<=$n; $j++)  
        {  
            echo '<input  style="width:200px" placeholder="chess value" type = "number" min="0" max="1" name = "board_arr[]">';  
        }  
        echo "</br>";  
    }  
}
?>
<html>
    <head>
      <style>
         .error {color: #FF0000;}
      </style>
   </head>
   
   <body>
      <?php
         $inputErr = "";
         $input = "";
         
         if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["input"])) {
               $inputErr = "Input is required";
            }elseif(!is_numeric($_POST["input"])){
                $inputErr = "Input should be numeric";
            }
         }
      ?>
      <h2>Program 2</h2>
      <p><span class = "error">* required field.</span></p>
      <form name="inputfrm" method = "post" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <table>
            <tr>
               <td>Input:</td>
               <td><input type = "text" name = "input">
                  <span class = "error">* <?php echo $inputErr;?></span>
               </td>
            </tr>
            <td>
               <input type = "submit" name = "submit" value = "Submit"> 
            </td>
         </table>
      </form>
      <?php if($_POST['input'] != ''){ ?>
      <form name="boardfrm" method = "post" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <table>
            <tr>
               <td>Size of Board:</td>
            </tr>
            <tr>
            <td><?php 
                echo generate_board($_POST['input']);
               ?></td>
               </tr>
            <td>
               <input type = "hidden" name = "input" value="<?php echo $_POST['input']; ?>">
               <input type = "submit" name = "boardsubmit" value = "Submit"> 
            </td>
         </table>
      </form>
    <?php } ?>
      <?php
      if(isset($_POST['boardsubmit']) && $_POST['boardsubmit'] != ''){
        // echo ' post values';
        // print_r($_POST);
        // echo "<h2>Your given values are as:</h2>";
        // echo '<pre>';print_r($_POST);'</pre>';
         $c = $_POST['board_arr']; 
        //echo '<pre>';print_r($c);'</pre>';
        $input = $_POST['input'];
        //echo "input >> ".$input;
        //$boardarr = array_chunk($c, 2);
        //$pieces = array_chunk($c, ceil(count($array) / 2));
        $boardarr = array_chunk($c, $input);
          //$c = array(array(1, 0), 
            //         array(0, 1)); 

          //echo 'Board Array';
          //echo '<pre>';print_r($boardarr);'</pre>';
          $input = (generate($boardarr,$_POST["input"])) ? "True" : "False";
          echo $input;
          echo "<br>";
      }
        
      ?>
   </body>
</html>