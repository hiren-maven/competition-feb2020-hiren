# Project Title

Write a program to determine whether given input will form chessboard pattern or not. Here "1" represents "White" and "0" represent "Black".

Sample Input 1
Size of Board: 2
1 0

1 1

Sample Output 1
false

Sample Input 2
Size of Board: 3
1 0 1

0 1 0

1 0 1

Sample Output 2
True

Important Note
If you are developing this programme in PHP / .NET then it must be web-based. CLI is not allowed
If you are developing this programme in Any JS Framework then it must be web-based. CLI is not allowed. Input and Output must work with JSON file like API call.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

- Download this files on your local machine where your server is running

## Running the tests

Run http://localhost/competition/1/1.php in browser. ( You can use your server url instead of localhost )
